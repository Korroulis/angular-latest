import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  FormGroup, FormControl, Validators, AbstractControl} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router} from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


  @Output () signupEvent: EventEmitter<string> = new EventEmitter();

  public passwordStrength: number = 0;
  public signupTitle = "Register to the super duper app";

  registerForm: FormGroup = new FormGroup ({
    username: new FormControl('',[Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(20)]),
    password: new FormControl('',[Validators.required,
                                  Validators.minLength(9),
                                  Validators.maxLength(20),
                                  Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')]),
    confirmPassword: new FormControl('',[Validators.required])
  }, this.passwordMatching);

  passwordMatching(frm: FormGroup) {
    return frm.get('password').value === frm.get('confirmPassword').value
       ? null : {'mismatch': true};
};

isRegistering:boolean = false;
registerError:string;
usernameReset:boolean = true;
initialValue:string;

  //constructor() { }
  constructor(private authService: AuthService, private router:Router, private session:SessionService) { }

  

  get username() {
    return this.registerForm.get('username');
  }

  get password() {
    return this.registerForm.get('password');
  }

  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  ngOnInit(): void {

    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }

  }

///// --- Signup button logic: If all the criteria for the username and passwor 
// are met and the passwords match each other then redirect to login ------

async  SignupClick() {

  this.initialValue = '';
  this.registerError = '';
    /* console.log(this.registerForm.value);
    console.log(this.registerForm.valid); 
    console.log(this.username.value);
    console.log(user); */
    try {
      this.isRegistering = true;
        if (this.registerForm.valid===true) {
          const result: any = await this.authService.register(this.registerForm.value);
          console.log(result);
          if (result.status<400) {
            this.session.save({
              token: result.data.token,
              username: result.data.user.username
            });
            this.router.navigateByUrl('/dashboard');
          }
        }
        else if (this.username.valid===false) {
          alert("Please insert a correct username");
        }
        else if (this.password.valid===false) {
          alert("Please insert a correct password");
        }
        else if (this.password.value!==this.confirmPassword.value) {
          alert("Password and Confirm Password do not match");
        } else {
          alert('Fill in the fields properly');
        }

      } catch (e) {
        
        /* console.log(this.registerForm); */
        this.isRegistering = false;
        this.registerError = e.error.error;
        this.usernameReset = false;
        this.registerForm.reset(this.initialValue);
      }
  } 

}
