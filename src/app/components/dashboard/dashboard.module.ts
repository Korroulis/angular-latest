import {NgModule} from "@angular/core";
import { Routes, RouterModule} from "@angular/router";
import {DashboardComponent} from './dashboard.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [
    {
        path:'',
        component: DashboardComponent
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes), CommonModule ],
    exports: [ RouterModule]
})

export class DashboardModule {}