import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  [x: string]: any;

  public loginTitle = "Login Form";

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('',[Validators.required,
                                  Validators.minLength(8),
                                  Validators.maxLength(20)]),
    password: new FormControl('',[Validators.required,
                                  Validators.minLength(9),
                                  Validators.maxLength(20)])
  });

  isLoading:boolean = false;
  loginError:string;

  constructor(private auth: AuthService, private router: Router, private session: SessionService) { }

  
  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  ngOnInit(): void {
    if (this.session.get() !== false) {
      this.router.navigateByUrl('/dashboard');
    }
  }

  async loginClick() {

    this.loginError = '';

    console.log(this.user);
    //console.log(this.loginForm.valid);

    try {
      this.isLoading = true;
      const result: any = await this.auth.login(this.loginForm.value);
      console.log(result);
      if (result.status <400) {
        this.session.save({token: result.data.token, username: result.data.user.username});
        this.router.navigateByUrl('/dashboard');
      }
    } catch (e) {
      //alert("Incorrect login credentials")
      this.isLoading = false;
      this.loginError = e.error.error;
      
    }

    /* const success = this.authService.login(userLogin);

    if (success) {
      this.router.navigateByUrl('/dashboard');
    } else {
      alert("You are not authorized to enter");
    }
    
  } */

}
}
