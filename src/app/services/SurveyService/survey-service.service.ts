import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../session/session.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SurveyServiceService {

  constructor(private http:HttpClient, private session: SessionService) { }

  getSurveys(): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys`, {
      headers: {
        'Authorization': 'Bearer ' + this.session.get().token
      }
    }).toPromise();
  }

  getSurvey(id): Promise<any> {
    return this.http.get(`${environment.apiUrl}/v1/api/surveys/${id}`).toPromise();
  }

}

